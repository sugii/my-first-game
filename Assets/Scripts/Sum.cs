﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))] 
public class Sum : MonoBehaviour {

	[SerializeField]
	private float speed;

	private Rigidbody2D myRigidbod;

	private Vector2 direction;

	// Use this for initialization
	void Start () {
		myRigidbod = GetComponent<Rigidbody2D> ();
	}

	void FixedUpdate()
	{
		myRigidbod.velocity = direction * speed;

	}

	public void Initialize(Vector2 direction)
	{
		this.direction = direction;
	}


	void OnBecameInvisible()
	{
		Destroy (gameObject);
	}
}
