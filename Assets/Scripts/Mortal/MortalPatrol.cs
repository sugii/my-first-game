﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MortalPatrol : IEnemyState {

	private Enemy enemy;

	private float patrolTimer;
	private float patrolDuraction; 

	public void Enter(Enemy enemy)
	{
		patrolDuraction = UnityEngine.Random.Range (1, 10);
		this.enemy = enemy;
	}

	public void Execute()
	{
		Patrol ();

		enemy.Move ();

		if(enemy.Target != null && enemy.InGunRange)
		{
			enemy.ChangeState (new RangedState());
		}
	}

	public void Exit()
	{

	}

	public void OnTriggerEnter(Collider2D other)
	{


	}

	private void Patrol()
	{
		patrolTimer += Time.deltaTime;

		if(patrolTimer >= patrolDuraction)
		{
			enemy.ChangeState (new MortalIdle());
		}
	}
}
