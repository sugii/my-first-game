﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MortalIdle : IEnemyState {

	private Enemy enemy;

	private float idleTimer;

	private float idleDuraction;

	public void Execute(){

		Idle ();

		if(enemy.Target != null)
		{
			enemy.ChangeState (new MortalRanged());
		}
	}
	public void Enter(Enemy enemy){
		idleDuraction = UnityEngine.Random.Range (1, 10);
		this.enemy = enemy;
	}
	public void Exit(){

	}
	public void OnTriggerEnter(Collider2D other){

	}

	public void Idle()
	{
		enemy.animator.SetFloat ("speed", 0);

		idleTimer += Time.deltaTime;

		if(idleTimer >= idleDuraction)
		{
			enemy.ChangeState (new MortalPatrol());
		}
	}

}
