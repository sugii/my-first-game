﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RangedState : IEnemyState {

	private Enemy enemy;

	private float gunTimer;
	private float gunCoolDown = 3;
	private bool canGun=true;

	public void Enter(Enemy enemy)
	{
		this.enemy = enemy;
	}

	public void Execute()
	{
		Gun ();
		if(enemy.InMeleeRange)
		{
			enemy.ChangeState (new MeleeState ());
		}

		if(enemy.im && enemy.healthStat.CurrentVal == 20)
		{
			enemy.ChangeState (new MortalIdle ());
			enemy.animator.SetTrigger ("im");
		}

		else if (enemy.Target != null) 
		{
			enemy.Move ();
		} 
		else
		{
			enemy.ChangeState (new IdleState ());
		}
	}

	public void Exit()
	{

	}

	public void OnTriggerEnter(Collider2D other)
	{

	}

	private void Gun()
	{
		gunTimer += Time.deltaTime;

		if(gunTimer >= gunCoolDown)
		{
			canGun = true;
			gunTimer = 0;
		}

		if(canGun)
		{
			canGun = false;
			enemy.animator.SetTrigger ("gun");
		}
	}

}
