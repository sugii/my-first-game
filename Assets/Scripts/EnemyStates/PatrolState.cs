﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PatrolState : IEnemyState {

	private Enemy enemy;

	private float patrolTimer;
	private float patrolDuraction; 

	public void Enter(Enemy enemy)
	{
		patrolDuraction = UnityEngine.Random.Range (1, 10);
		this.enemy = enemy;
	}

	public void Execute()
	{
		Patrol ();

		enemy.Move ();

		if(enemy.im && enemy.healthStat.CurrentVal == 20)
		{
			enemy.ChangeState (new MortalIdle ());
			enemy.animator.SetTrigger ("im");
		}

		if(enemy.Target != null && enemy.InGunRange)
		{
			enemy.ChangeState (new RangedState());
		}
	}

	public void Exit()
	{

	}

	public void OnTriggerEnter(Collider2D other)
	{
		
		if(other.tag == "Sum")
		{
			enemy.Target = Player.Instance.gameObject;
		}
	}

	private void Patrol()
	{
		patrolTimer += Time.deltaTime;

		if(patrolTimer >= patrolDuraction)
		{
			enemy.ChangeState (new IdleState());
		}
	}

}
