﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IdleState : IEnemyState {

	private Enemy enemy;

	private float idleTimer;

	private float idleDuraction;

	public void Enter(Enemy enemy)
	{
		idleDuraction = UnityEngine.Random.Range (1, 10);
		this.enemy = enemy;
	}

	public void Execute()
	{
		Idle ();


		if(enemy.im && enemy.healthStat.CurrentVal == 20)
		{
			enemy.ChangeState (new MortalIdle ());
			enemy.animator.SetTrigger ("im");
		}
		else if(enemy.Target != null)
		{
			enemy.ChangeState (new RangedState());
		}
	}

	public void Exit()
	{

	}

	public void OnTriggerEnter(Collider2D other)
	{
		if(other.tag == "Sum")
		{
			enemy.Target = Player.Instance.gameObject;
		}
	}

	public void Idle()
	{
		enemy.animator.SetFloat ("speed", 0);

		idleTimer += Time.deltaTime;

		if(idleTimer >= idleDuraction)
		{
			enemy.ChangeState (new PatrolState());
		}
	}

}
