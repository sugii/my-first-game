﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public abstract class Character : MonoBehaviour {

	public Animator animator { get; private set;}


	[SerializeField]
	public Stat healthStat;

	[SerializeField]
	protected Transform GunPos;

	[SerializeField]
	protected float moveSpeed;

	protected bool facingRight;

	[SerializeField]
	protected GameObject knifePrefab;

	[SerializeField]
	private EdgeCollider2D swordCollider;

	public EdgeCollider2D SwordCollider
	{
		get 
		{
			return swordCollider;
		}
	}

	[SerializeField]
	private List<string> damageSources;

	public abstract bool IsDead{ get; }

	public bool Attack { get; set; }

	public bool TakingDamage { get; set;}

	// Use this for initialization
	public virtual void Start () 
	{
		facingRight = true;

		animator = GetComponent<Animator> ();
		healthStat.Initialize ();
	}
	
	// Update is called once per frame
	void Update () {
		
	}
		

	public abstract IEnumerator TakeDamage ();

	public abstract void Death ();

	public virtual void ChangeDirection()
	{
		facingRight = !facingRight;
		transform.localScale = new Vector3 (transform.localScale.x * -1, 1, 1);
	}

	public virtual void Gun(int value)
	{
		if (facingRight) 
		{
			GameObject tmp = (GameObject) Instantiate (knifePrefab, GunPos.position, Quaternion.identity);
			tmp.GetComponent<Sum> ().Initialize (Vector2.right);
		}
		else
		{
			GameObject tmp = (GameObject) Instantiate (knifePrefab, GunPos.position, Quaternion.identity);
			tmp.GetComponent<Sum> ().Initialize (Vector2.left);
		}
	}

	public void MeleeAttack()
	{
		SwordCollider.enabled = true;
	}

	public virtual void OnTriggerEnter2D(Collider2D other)
	{
		if(damageSources.Contains(other.tag))
		{
			StartCoroutine (TakeDamage());
		}
	}



}
